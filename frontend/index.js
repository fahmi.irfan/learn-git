// import express
const express = require('express')
const axios = require('axios')

// create app instance
const app = express()

app.get("/ping", (req, res) => {
    res.send("pong")
})

app.get("/news", (req, res) => {
    axios.get('http://api:3000/posts')
     .then(({ data }) => {
        console.log("data", data)
        res.status(200).json(data)
    })
    .catch(e => {
        console.error(e)
        res.status(400).json(e)
    })
})

// listening request on port 3000 on container
app.listen("3000", () => {
    console.log("app run on port 3000")
})
